void runner() {
  gROOT->ProcessLine(".exception");
  gSystem->SetFlagsDebug(gSystem->Getenv("CPPEXPFLAGS"));
  gSystem->SetFlagsOpt(gSystem->Getenv("CPPEXPFLAGS"));
  gROOT->ProcessLine(".L histoFiller.C+");
  string line;
  ifstream myfile("samples.txt");
  if (!myfile.is_open()) return;
  while (getline(myfile,line)) {
    std::cout << "Current File " << line << '\n';
    histoFiller x(0,line);
    x.Loop();
  }
}
