//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Aug 26 19:00:29 2016 by ROOT version 5.34/36
// from TTree jvttree/jvttree
// found on file: /eos/atlas/user/m/maklein/tagprobe/zero.root
//////////////////////////////////////////////////////////

#ifndef histoFiller_h
#define histoFiller_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class histoFiller {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         muon1pt;
   Float_t         muon2pt;
   Float_t         muon1eta;
   Float_t         muon2eta;
   Float_t         muon1phi;
   Float_t         muon2phi;
   Float_t         weight;
   Float_t         xweight;
   Float_t         prw;
   Float_t         muscale;
   Float_t         NPV;
   Float_t         MU;
   Float_t         VERT;
   Int_t           GRL;
   Int_t           identification;
   Int_t           randomRunNumber;
   Int_t           pvind0;
   Int_t           pvind1;
   Float_t         metx;
   Float_t         mety;
   Float_t         mets;
   Float_t         tmetx;
   Float_t         tmety;
   vector<float>   *jetpt;
   vector<float>   *jeteta;
   vector<float>   *jetphi;
   vector<float>   *muonpt;
   vector<float>   *muoneta;
   vector<float>   *muonphi;
   vector<float>   *muond0;
   vector<float>   *muonz0;
   vector<float>   *muonqual;
   vector<float>   *matchpt;
   vector<float>   *matcheta;
   vector<float>   *matchphi;
   vector<int>     *matchvert;
   vector<float>   *jetJVT;
   vector<float>   *jetDRPT;
   vector<float>   *jetRPT;
   vector<float>   *jetJVF;
   vector<vector<float> > *jetSUMS;
   vector<float>   *jetwidth;
   vector<float>   *jetcombwidth;
   vector<float>   *jetfjvt2;
   vector<float>   *jetfjvt;
   vector<float>   *jetfjvt975;
   vector<float>   *jetfjvt95;
   vector<float>   *jetfjvt925;
   vector<float>   *jetfjvt90;
   vector<float>   *jetfjvt875;
   vector<float>   *jetfjvt85;
   vector<float>   *jettiming;
   vector<float>   *jetfjvt2b;
   vector<int>     *jetvert;
   vector<int>     *jettype;
   vector<float>   *jettruth;
   vector<vector<float> > *jetsource;
   vector<vector<float> > *jethssource;
   vector<int>     *jetflavor;

   // List of branches
   TBranch        *b_muon1pt;   //!
   TBranch        *b_muon2pt;   //!
   TBranch        *b_muon1eta;   //!
   TBranch        *b_muon2eta;   //!
   TBranch        *b_muon1phi;   //!
   TBranch        *b_muon2phi;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_xweight;   //!
   TBranch        *b_prw;   //!
   TBranch        *b_muscale;   //!
   TBranch        *b_NPV;   //!
   TBranch        *b_MU;   //!
   TBranch        *b_VERT;   //!
   TBranch        *b_GRL;   //!
   TBranch        *b_identification;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_pvind0;   //!
   TBranch        *b_pvind1;   //!
   TBranch        *b_metx;   //!
   TBranch        *b_mety;   //!
   TBranch        *b_mets;   //!
   TBranch        *b_tmetx;   //!
   TBranch        *b_tmety;   //!
   TBranch        *b_jetpt;   //!
   TBranch        *b_jeteta;   //!
   TBranch        *b_jetphi;   //!
   TBranch        *b_muonpt;   //!
   TBranch        *b_muoneta;   //!
   TBranch        *b_muonphi;   //!
   TBranch        *b_muond0;   //!
   TBranch        *b_muonz0;   //!
   TBranch        *b_muonqual;   //!
   TBranch        *b_matchpt;   //!
   TBranch        *b_matcheta;   //!
   TBranch        *b_matchphi;   //!
   TBranch        *b_matchvert;   //!
   TBranch        *b_jetJVT;   //!
   TBranch        *b_jetDRPT;   //!
   TBranch        *b_jetRPT;   //!
   TBranch        *b_jetJVF;   //!
   TBranch        *b_jetSUMS;   //!
   TBranch        *b_jetwidth;   //!
   TBranch        *b_jetcombwidth;   //!
   TBranch        *b_jetfjvt2;   //!
   TBranch        *b_jetfjvt;   //!
   TBranch        *b_jetfjvt975;   //!
   TBranch        *b_jetfjvt95;   //!
   TBranch        *b_jetfjvt925;   //!
   TBranch        *b_jetfjvt90;   //!
   TBranch        *b_jetfjvt875;   //!
   TBranch        *b_jetfjvt85;   //!
   TBranch        *b_jettiming;   //!
   TBranch        *b_jetfjvt2b;   //!
   TBranch        *b_jetvert;   //!
   TBranch        *b_jettype;   //!
   TBranch        *b_jettruth;   //!
   TBranch        *b_jetsource;   //!
   TBranch        *b_jethssource;   //!
   TBranch        *b_jetflavor;   //!
   bool isdata;//!
   bool iszero;//!
   TString thisName;//!

   histoFiller(TTree *tree=0,TString filename="");
   virtual ~histoFiller();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int jvtEff = 1);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef histoFiller_cxx
histoFiller::histoFiller(TTree *tree,TString filename) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   isdata = false;
   iszero = false;
   thisName = filename;
   filename.Prepend("/eos/atlas/user/m/maklein/fjvtCalib3/");
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(filename.Data());
      if (!f || !f->IsOpen()) {
         f = new TFile(filename.Data());
      }
      f->GetObject("jvttree",tree);
      if (TString(f->GetName()).Contains("zero") || TString(f->GetName()).Contains("data")) isdata = true;
      if (TString(f->GetName()).Contains("zero") || TString(f->GetName()).Contains("361020") || TString(f->GetName()).Contains("424005")) iszero = true;

   }
   Init(tree);
}

histoFiller::~histoFiller()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t histoFiller::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t histoFiller::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void histoFiller::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jetpt = 0;
   jeteta = 0;
   jetphi = 0;
   muonpt = 0;
   muoneta = 0;
   muonphi = 0;
   muond0 = 0;
   muonz0 = 0;
   muonqual = 0;
   matchpt = 0;
   matcheta = 0;
   matchphi = 0;
   matchvert = 0;
   jetJVT = 0;
   jetDRPT = 0;
   jetRPT = 0;
   jetJVF = 0;
   jetSUMS = 0;
   jetwidth = 0;
   jetcombwidth = 0;
   jetfjvt2 = 0;
   jetfjvt = 0;
   jetfjvt975 = 0;
   jetfjvt95 = 0;
   jetfjvt925 = 0;
   jetfjvt90 = 0;
   jetfjvt875 = 0;
   jetfjvt85 = 0;
   jettiming = 0;
   jetfjvt2b = 0;
   jetvert = 0;
   jettype = 0;
   jettruth = 0;
   jetsource = 0;
   jethssource = 0;
   jetflavor = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("muon1pt", &muon1pt, &b_muon1pt);
   fChain->SetBranchAddress("muon2pt", &muon2pt, &b_muon2pt);
   fChain->SetBranchAddress("muon1eta", &muon1eta, &b_muon1eta);
   fChain->SetBranchAddress("muon2eta", &muon2eta, &b_muon2eta);
   fChain->SetBranchAddress("muon1phi", &muon1phi, &b_muon1phi);
   fChain->SetBranchAddress("muon2phi", &muon2phi, &b_muon2phi);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("xweight", &xweight, &b_xweight);
   fChain->SetBranchAddress("prw", &prw, &b_prw);
   fChain->SetBranchAddress("muscale", &muscale, &b_muscale);
   fChain->SetBranchAddress("NPV", &NPV, &b_NPV);
   fChain->SetBranchAddress("MU", &MU, &b_MU);
   fChain->SetBranchAddress("VERT", &VERT, &b_VERT);
   fChain->SetBranchAddress("GRL", &GRL, &b_GRL);
   fChain->SetBranchAddress("identification", &identification, &b_identification);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("pvind0", &pvind0, &b_pvind0);
   fChain->SetBranchAddress("pvind1", &pvind1, &b_pvind1);
   fChain->SetBranchAddress("metx", &metx, &b_metx);
   fChain->SetBranchAddress("mety", &mety, &b_mety);
   fChain->SetBranchAddress("mets", &mets, &b_mets);
   fChain->SetBranchAddress("tmetx", &tmetx, &b_tmetx);
   fChain->SetBranchAddress("tmety", &tmety, &b_tmety);
   fChain->SetBranchAddress("jetpt", &jetpt, &b_jetpt);
   fChain->SetBranchAddress("jeteta", &jeteta, &b_jeteta);
   fChain->SetBranchAddress("jetphi", &jetphi, &b_jetphi);
   fChain->SetBranchAddress("muonpt", &muonpt, &b_muonpt);
   fChain->SetBranchAddress("muoneta", &muoneta, &b_muoneta);
   fChain->SetBranchAddress("muonphi", &muonphi, &b_muonphi);
   fChain->SetBranchAddress("muond0", &muond0, &b_muond0);
   fChain->SetBranchAddress("muonz0", &muonz0, &b_muonz0);
   fChain->SetBranchAddress("muonqual", &muonqual, &b_muonqual);
   fChain->SetBranchAddress("matchpt", &matchpt, &b_matchpt);
   fChain->SetBranchAddress("matcheta", &matcheta, &b_matcheta);
   fChain->SetBranchAddress("matchphi", &matchphi, &b_matchphi);
   fChain->SetBranchAddress("matchvert", &matchvert, &b_matchvert);
   fChain->SetBranchAddress("jetJVT", &jetJVT, &b_jetJVT);
   fChain->SetBranchAddress("jetDRPT", &jetDRPT, &b_jetDRPT);
   fChain->SetBranchAddress("jetRPT", &jetRPT, &b_jetRPT);
   fChain->SetBranchAddress("jetJVF", &jetJVF, &b_jetJVF);
   fChain->SetBranchAddress("jetSUMS", &jetSUMS, &b_jetSUMS);
   fChain->SetBranchAddress("jetwidth", &jetwidth, &b_jetwidth);
   fChain->SetBranchAddress("jetcombwidth", &jetcombwidth, &b_jetcombwidth);
   fChain->SetBranchAddress("jetfjvt2", &jetfjvt2, &b_jetfjvt2);
   fChain->SetBranchAddress("jetfjvt", &jetfjvt, &b_jetfjvt);
   fChain->SetBranchAddress("jetfjvt975", &jetfjvt975, &b_jetfjvt975);
   fChain->SetBranchAddress("jetfjvt95", &jetfjvt95, &b_jetfjvt95);
   fChain->SetBranchAddress("jetfjvt925", &jetfjvt925, &b_jetfjvt925);
   fChain->SetBranchAddress("jetfjvt90", &jetfjvt90, &b_jetfjvt90);
   fChain->SetBranchAddress("jetfjvt875", &jetfjvt875, &b_jetfjvt875);
   fChain->SetBranchAddress("jetfjvt85", &jetfjvt85, &b_jetfjvt85);
   fChain->SetBranchAddress("jettiming", &jettiming, &b_jettiming);
   fChain->SetBranchAddress("jetfjvt2b", &jetfjvt2b, &b_jetfjvt2b);
   fChain->SetBranchAddress("jetvert", &jetvert, &b_jetvert);
   fChain->SetBranchAddress("jettype", &jettype, &b_jettype);
   fChain->SetBranchAddress("jettruth", &jettruth, &b_jettruth);
   fChain->SetBranchAddress("jetsource", &jetsource, &b_jetsource);
   fChain->SetBranchAddress("jethssource", &jethssource, &b_jethssource);
   fChain->SetBranchAddress("jetflavor", &jetflavor, &b_jetflavor);
   Notify();
}

Bool_t histoFiller::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void histoFiller::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t histoFiller::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef histoFiller_cxx
