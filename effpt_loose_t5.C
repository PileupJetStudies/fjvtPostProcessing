#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include <TCanvas.h>
void effpt_loose_t5() {
   SetAtlasStyle();
  TCanvas* c = new TCanvas("c","single inclusive jets",50,50,600,600);
   TFile* file1 = new TFile("hist_zero.root");
   TProfile *dataplot = ((TProfile*)gDirectory->Get("distt5pt"));
   TFile* file2 = new TFile("hist_424005.root");
   TProfile *mcplot = ((TProfile*)gDirectory->Get("distt5pt"));

   dataplot->SetLineColor(1);
   mcplot->SetLineColor(2);
   dataplot->SetMarkerColor(1);
   mcplot->SetMarkerColor(2);
   mcplot->SetMarkerStyle(21);
  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
  pad1->SetBottomMargin(0.03);
  pad1->Draw();
  pad1->cd();
   TH2D *axor = new TH2D("axor","axor",4,20,120,100,0.0,1.2);
   axor->GetYaxis()->SetTitle("Jet Efficiency");
   axor->GetXaxis()->SetLabelOffset(1);
   axor->GetXaxis()->SetLabelOffset(1.);
   axor->GetYaxis()->SetTitleSize(0.08);
   axor->GetYaxis()->SetTitleOffset(1.);
   axor->GetXaxis()->SetLabelSize(0.07);
   axor->GetYaxis()->SetLabelSize(0.07);
   axor->Draw();
   dataplot->Draw("SAME pe0");
   mcplot->Draw("SAME pe0");
   pad1->RedrawAxis();
  TLegend *leg_STVF = new TLegend(0.48, 0.28, 0.8, 0.47);    
  leg_STVF->SetFillColor(0);
  leg_STVF->SetShadowColor(0); 
  leg_STVF->SetBorderSize(0);
  leg_STVF->SetTextSize(0.06);
  leg_STVF->SetFillStyle(0);
  leg_STVF->SetTextFont(42);
  char message[100]; 
  sprintf(message,"Data"); 
  leg_STVF->AddEntry(dataplot,message,"p"); 
  sprintf(message,"Pythia8 MC"); 
  leg_STVF->AddEntry(mcplot,message,"p"); 
  leg_STVF->Draw();
  ATLASLabel(0.20,0.86,"Internal");
  myText(       0.20, 0.80, 1, "Zero bias",0.06);
  myText(       0.2, 0.74, 1, "#sqrt{s} = 13 TeV",0.06);
  myText(       0.2, 0.68, 1, "Anti-#font[52]{k}_{t} EM+JES R=0.4",0.06);
  myText(       0.2, 0.62, 1, "2.5<|#eta|<4.5, |t|<10 ns",0.06);
  myText(       0.2, 0.56, 1, "92% Efficiency",0.06);

  c->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0.0,1,0.3);
  pad2->SetTopMargin(0.03);
  pad2->SetBottomMargin(0.35);
  //pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
   TH2D *axor2 = new TH2D("axor2","axor2",4,20,120,100,0.8,1.2);
  axor2->GetYaxis()->SetNdivisions(505);
  axor2->GetYaxis()->SetLabelSize(0.15);
  axor2->GetXaxis()->SetLabelSize(0.15);
  axor2->SetTitleSize(0.15,"X");
  axor2->SetTitleSize(0.15,"Y");
  axor2->GetXaxis()->SetTitleOffset(1.);
  axor2->GetYaxis()->SetTitleOffset(0.5);
  axor2->GetYaxis()->SetTitle("MC/Data");
  axor2->GetXaxis()->SetTitle("p_{T} [GeV]");
   axor2->Draw();
  TLine *line = new TLine(20,1,60,1);
  line->SetLineColor(2);
  //line->Draw("SAME");

  TH1D *newdata = dataplot->ProjectionX("newdata");
  TH1D *newmc = mcplot->ProjectionX("newmc");
  newmc->SetLineColor(mcplot->GetLineColor());
  newmc->SetMarkerColor(mcplot->GetMarkerColor());
  newmc->SetMarkerStyle(mcplot->GetMarkerStyle());
  newmc->Divide(newdata);
  newdata->Divide(newdata);

  newmc->Draw("SAME pe0");
  newdata->Draw("hist SAME");
  //mcplot->Draw();
  //dataplot->Draw("SAME");
  pad2->RedrawAxis();
  c->cd();
  TImage *img = TImage::Create();
  img->FromPad(c);
  img->WriteImage("effpt_loose_t5.png"); 
  c->Print("effpt_loose_t5.eps");
   gPad->WaitPrimitive();
   

}
