#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include <TCanvas.h>
void effhspt2(bool dotime = false,int npv=0,int pass = 1) {
   SetAtlasStyle();
  TCanvas* c = new TCanvas("c","single inclusive jets",50,50,600,600);
   TString histo = "distpt";
   if (dotime) histo.ReplaceAll("dist","distt5");
   if (pass==2) histo.ReplaceAll("dist","disttight");
   if (npv) histo += npv;
gStyle->SetErrorX(0.001);

   TFile* file1back = new TFile("hist_zero.root");
   TH1D *zerobiasplotAll = ((TProfile*)gDirectory->Get(histo.Data()))->ProjectionX("_px0","B");
   TH1D *zerobiasplotPass = ((TProfile*)gDirectory->Get(histo.Data()))->ProjectionX("_px1","W");
   TH1D *zerobiasplot = ((TProfile*)gDirectory->Get(histo.Data()));
   zerobiasplotAll->SetName("zerobiasplotAll");
   TH1D *zerobiasnpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));

   TFile* file2back = new TFile("hist_424005.root");
   TH1D *minbiasplotAll = ((TProfile*)gDirectory->Get(histo.Data()))->ProjectionX("_px2","B");
   TH1D *minbiasplotPass = ((TProfile*)gDirectory->Get(histo.Data()))->ProjectionX("_px3","W");
   TH1D *minbiasplot = ((TProfile*)gDirectory->Get(histo.Data()));
   minbiasplotAll->SetName("minbiasplotAll");
   TH1D *minbiasnpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));

   minbiasplotAll->Scale(1-2.8/TMath::Pi());
   minbiasplotPass->Scale(1-2.8/TMath::Pi());
   zerobiasplotAll->Scale(1-2.8/TMath::Pi());
   zerobiasplotPass->Scale(1-2.8/TMath::Pi());


   TFile* file1 = new TFile("hist_data.root");
   TProfile *dataplot3 = (TProfile*)((TProfile*)gDirectory->Get(histo.Data()))->Clone("distmcpt3");
   TProfile *pdataplot = ((TProfile*)gDirectory->Get(histo.Data()));
   TH1D *datanpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));

   TFile* file2 = new TFile("hist_361107.root");
   TProfile *mcplot3 = (TProfile*)((TProfile*)gDirectory->Get(histo.Data()))->Clone("distdatapt3");
   TProfile *pmcplot = ((TProfile*)gDirectory->Get(histo.Data()));
   TH1D *mcnpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));
   TFile* file3 = new TFile("hist_mad.root");
   TProfile *madplot3 = (TProfile*)((TProfile*)gDirectory->Get(histo.Data()))->Clone("distdatapt4");
   TProfile *pmadplot = ((TProfile*)gDirectory->Get(histo.Data()));
   TH1D *madnpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));
   TFile* file4 = new TFile("hist_sherpa.root");
   TProfile *sherpaplot3 = (TProfile*)((TProfile*)gDirectory->Get(histo.Data()))->Clone("distdatapt5");
   TProfile *psherpaplot = ((TProfile*)gDirectory->Get(histo.Data()));
   TH1D *sherpanpvplot = ((TH1D*)gDirectory->Get("npvafterdist"));

   TH1D *minbiasMadplotAll = (TH1D*)minbiasplotAll->Clone("minbiasMadplotAll");
   TH1D *minbiasMadplotPass = (TH1D*)minbiasplotPass->Clone("minbiasMadplotPass");
   TH1D *minbiasSherpaplotAll = (TH1D*)minbiasplotAll->Clone("minbiasSherpaplotAll");
   TH1D *minbiasSherpaplotPass = (TH1D*)minbiasplotPass->Clone("minbiasSherpaplotPass");

   minbiasplotAll->Scale(mcnpvplot->Integral(npv,!npv?mcnpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   minbiasplotPass->Scale(mcnpvplot->Integral(npv,!npv?mcnpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   minbiasMadplotAll->Scale(madnpvplot->Integral(npv,!npv?madnpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   minbiasMadplotPass->Scale(madnpvplot->Integral(npv,!npv?madnpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   minbiasSherpaplotAll->Scale(sherpanpvplot->Integral(npv,!npv?sherpanpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   minbiasSherpaplotPass->Scale(sherpanpvplot->Integral(npv,!npv?sherpanpvplot->GetNbinsX()+1:npv+9)/minbiasnpvplot->Integral(npv,!npv?minbiasnpvplot->GetNbinsX()+1:npv+9));
   zerobiasplotAll->Scale(datanpvplot->Integral(npv,!npv?datanpvplot->GetNbinsX()+1:npv+9)/zerobiasnpvplot->Integral(npv,!npv?zerobiasnpvplot->GetNbinsX()+1:npv+9));
   zerobiasplotPass->Scale(datanpvplot->Integral(npv,!npv?datanpvplot->GetNbinsX()+1:npv+9)/zerobiasnpvplot->Integral(npv,!npv?zerobiasnpvplot->GetNbinsX()+1:npv+9));

   TH1D *dataplotPass = pdataplot->ProjectionX("pdataplotPass","W");
   TH1D *dataplotAll = pdataplot->ProjectionX("pdataplotAll","B");
   TH1D *mcplotPass = pmcplot->ProjectionX("pmcplotPass","W");
   TH1D *mcplotAll = pmcplot->ProjectionX("pmcplotAll","B");
   TH1D *madplotPass = pmadplot->ProjectionX("pmadplotPass","W");
   TH1D *madplotAll = pmadplot->ProjectionX("pmadplotAll","B");
   TH1D *sherpaplotPass = psherpaplot->ProjectionX("psherpaplotPass","W");
   TH1D *sherpaplotAll = psherpaplot->ProjectionX("psherpaplotAll","B");

   TH1D *mcplot = (TH1D*)mcplotPass->Clone("mcplot");
   TH1D *madplot = (TH1D*)madplotPass->Clone("madplot");
   TH1D *sherpaplot = (TH1D*)sherpaplotPass->Clone("sherpaplot");
   TH1D *dataplot = (TH1D*)dataplotPass->Clone("dataplot");
   for (size_t i = 1; i <= mcplot->GetNbinsX(); i++) {
     //mcplot->SetBinContent(i,(mcplot3->GetBinContent(i)*mcplotAll->GetBinContent(i)-0.9*minbiasplot->GetBinContent(i)*minbiasplotAll->GetBinContent(i))/(mcplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i)));
     //dataplot->SetBinContent(i,(dataplot3->GetBinContent(i)*dataplotAll->GetBinContent(i)-0.9*zerobiasplot->GetBinContent(i)*zerobiasplotAll->GetBinContent(i))/(dataplotAll->GetBinContent(i)-0.9*zerobiasplotAll->GetBinContent(i)));
     mcplot->SetBinContent(i,(mcplot3->GetBinContent(i)*mcplotAll->GetBinContent(i)-0.9*minbiasplot->GetBinContent(i)*minbiasplotAll->GetBinContent(i))/(mcplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i)>0?mcplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i):1));
     dataplot->SetBinContent(i,(dataplot3->GetBinContent(i)*dataplotAll->GetBinContent(i)-0.9*zerobiasplot->GetBinContent(i)*zerobiasplotAll->GetBinContent(i))/(dataplotAll->GetBinContent(i)-0.9*zerobiasplotAll->GetBinContent(i)>0?dataplotAll->GetBinContent(i)-0.9*zerobiasplotAll->GetBinContent(i):1));
     double errp1 = mcplot3->GetBinError(i)*mcplotAll->GetBinContent(i)/(mcplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i));
     double errp2 = 0.9*minbiasplot->GetBinError(i)*minbiasplotAll->GetBinContent(i)/(mcplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i));
     double errp3 = fabs(mcplot->GetBinContent(i)-(mcplot3->GetBinContent(i)*mcplotAll->GetBinContent(i)-1.0*minbiasplot->GetBinContent(i)*minbiasplotAll->GetBinContent(i))/(mcplotAll->GetBinContent(i)-1.0*minbiasplotAll->GetBinContent(i)));
     double errp4 = fabs(mcplot->GetBinContent(i)-(mcplot3->GetBinContent(i)*mcplotAll->GetBinContent(i)-0.8*minbiasplot->GetBinContent(i)*minbiasplotAll->GetBinContent(i))/(mcplotAll->GetBinContent(i)-0.8*minbiasplotAll->GetBinContent(i)));
     double errp5 = fabs(mcplot->GetBinContent(i)-(madplot3->GetBinContent(i)*madplotAll->GetBinContent(i)-1.0*minbiasplot->GetBinContent(i)*minbiasMadplotAll->GetBinContent(i))/(madplotAll->GetBinContent(i)-1.0*minbiasMadplotAll->GetBinContent(i)));
     double errp6 = fabs(mcplot->GetBinContent(i)-(sherpaplot3->GetBinContent(i)*sherpaplotAll->GetBinContent(i)-1.0*minbiasplot->GetBinContent(i)*minbiasSherpaplotAll->GetBinContent(i))/(sherpaplotAll->GetBinContent(i)-1.0*minbiasSherpaplotAll->GetBinContent(i)));


     double errm1 = dataplot3->GetBinError(i)*dataplotAll->GetBinContent(i)/(dataplotAll->GetBinContent(i)-0.9*minbiasplotAll->GetBinContent(i));
     double errm2 = 0.9*zerobiasplot->GetBinError(i)*zerobiasplotAll->GetBinContent(i)/(dataplotAll->GetBinContent(i)-0.9*zerobiasplotAll->GetBinContent(i));
     double errm3 = fabs(dataplot->GetBinContent(i)-(dataplot3->GetBinContent(i)*dataplotAll->GetBinContent(i)-1.*zerobiasplot->GetBinContent(i)*zerobiasplotAll->GetBinContent(i))/(dataplotAll->GetBinContent(i)-1.*zerobiasplotAll->GetBinContent(i)));
     double errm4 = fabs(dataplot->GetBinContent(i)-(dataplot3->GetBinContent(i)*dataplotAll->GetBinContent(i)-0.8*zerobiasplot->GetBinContent(i)*zerobiasplotAll->GetBinContent(i))/(dataplotAll->GetBinContent(i)-0.8*zerobiasplotAll->GetBinContent(i)));
     //mcplot->SetBinError(i,sqrt(pow(errp1,2)+pow(errp2,2)+pow(errp3,2)+pow(errp4,2)+pow(errp5,2)+pow(errp6,2)));
     mcplot->SetBinError(i,sqrt(pow(errp1,2)+pow(errp2,2)+pow(errp3,2)+pow(errp4,2)+pow(errp6,2)));
     //mcplot->SetBinError(i,sqrt(pow(errp1,2)+pow(errp2,2)+pow(errp3,2)+pow(errp4,2)));
     dataplot->SetBinError(i,sqrt(pow(errm1,2)+pow(errm2,2)+pow(errm3,2)+pow(errm4,2)));
   }

   //dataplotPass->Add(zerobiasplotPass,-1);
   //dataplotAll->Add(zerobiasplotAll,-1);
   //mcplotPass->Add(minbiasplotPass,-1);
   //mcplotAll->Add(minbiasplotAll,-1);
   
   //TH1D *dataplot = (TH1D*)dataplotPass->Clone("dataplot");
   //TH1D *mcplot = (TH1D*)mcplotPass->Clone("mcplot");
   //dataplot->Divide(dataplotAll);
   //mcplot->Divide(mcplotAll);

   dataplot->SetLineColor(1);
   mcplot->SetLineColor(2);
   dataplot->SetMarkerColor(1);
   mcplot->SetMarkerColor(2);
   mcplot->SetMarkerStyle(21);
  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
  pad1->SetBottomMargin(0.03);
  pad1->Draw();
  pad1->cd();
   TH2D *axor = new TH2D("axor","axor",4,20,120,100,0.,1.5);
   axor->GetYaxis()->SetTitle("Hard-scatter Jet Efficiency");
   axor->GetXaxis()->SetLabelOffset(1);
   axor->GetXaxis()->SetLabelOffset(1.);
   axor->GetYaxis()->SetTitleSize(0.08);
   axor->GetYaxis()->SetTitleOffset(1.);
   axor->GetXaxis()->SetLabelSize(0.07);
   axor->GetYaxis()->SetLabelSize(0.07);
   axor->Draw();
   std::cout << "SDD " << pdataplot->GetBinContent(4) << " " << pmcplot->GetBinContent(4) << '\n';
   dataplot->DrawCopy("SAME pe0");
   mcplot->DrawCopy("SAME pe0");
   pad1->RedrawAxis();
  TLegend *leg_STVF = new TLegend(0.48, 0.08, 0.8, 0.27);    
  leg_STVF->SetFillColor(0);
  leg_STVF->SetShadowColor(0); 
  leg_STVF->SetBorderSize(0);
  leg_STVF->SetTextSize(0.06);
  leg_STVF->SetFillStyle(0);
  leg_STVF->SetTextFont(42);
  char message[100]; 
  sprintf(message,"Data"); 
  leg_STVF->AddEntry(dataplot,message,"p"); 
  sprintf(message,"Powheg+Pythia8 MC"); 
  leg_STVF->AddEntry(mcplot,message,"p"); 
  leg_STVF->Draw();
  ATLASLabel(0.20,0.86,"Internal");
  myText(       0.20, 0.80, 1, "Z+jets, #sqrt{s} = 13 TeV",0.06);
  myText(       0.2, 0.74, 1, "2.5<|#eta|<4.5",0.06);
  if (dotime) myText(       0.2, 0.68, 1, "|t|<10 ns",0.06);
  if (npv==10) myText(       0.2, 0.32, 1, "10#leqn_{PV}<20",0.06);
  if (npv==20) myText(       0.2, 0.32, 1, "20#leqn_{PV}<30",0.06);
  if (npv==30) myText(       0.2, 0.32, 1, "30#leqn_{PV}<40",0.06);
  if (npv==40) myText(       0.2, 0.32, 1, "40#leqn_{PV}<50",0.06);
  if (pass==1) myText(       0.2, 0.26, 1, "fJVT<0.5",0.06);
  if (pass==2) myText(       0.2, 0.26, 1, "fJVT<0.4",0.06);

  c->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0.0,1,0.3);
  pad2->SetTopMargin(0.03);
  pad2->SetBottomMargin(0.35);
  //pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
  pad2->SetGridy();
   TH2D *axor2 = new TH2D("axor2","axor2",4,20,120,100,0.94,1.01);
  axor2->GetYaxis()->SetNdivisions(505);
  axor2->GetYaxis()->SetLabelSize(0.15);
  axor2->GetXaxis()->SetLabelSize(0.15);
  axor2->SetTitleSize(0.15,"X");
  axor2->SetTitleSize(0.15,"Y");
  axor2->GetXaxis()->SetTitleOffset(1.);
  axor2->GetYaxis()->SetTitleOffset(0.5);
  axor2->GetYaxis()->SetTitle("MC/Data");
  axor2->GetXaxis()->SetTitle("p_{T} [GeV]");
   axor2->Draw();
  TLine *line = new TLine(20,1,200,1);
  line->SetLineColor(2);
  //line->Draw("SAME");

  mcplot->SetLineColor(mcplot->GetLineColor());
  mcplot->SetMarkerColor(mcplot->GetMarkerColor());
  mcplot->SetMarkerStyle(mcplot->GetMarkerStyle());
  mcplot->Divide(dataplot);
  dataplot->Divide(dataplot);

  mcplot->Draw("SAME pe0");
  dataplot->Draw("hist SAME");
  //mcplot->Draw();
  //dataplot->Draw("SAME");
  pad2->RedrawAxis();
  c->cd();
  TImage *img = TImage::Create();
  img->FromPad(c);
  TString  filename = "effhspt2";
  if (dotime) filename += "t5";
  if (npv) filename += "_";
  if (npv) filename += npv;
  if (pass) filename += "_";
  if (pass) filename += pass;
  filename += ".png";
  img->WriteImage(filename.Data()); 
  //c->Print("distnum.eps");
   gPad->WaitPrimitive();

}
